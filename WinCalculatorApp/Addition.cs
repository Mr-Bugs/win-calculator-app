﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinCalculatorApp
{
    public class Addition : Operator
    {
        public override int Priority => 1;

        public override double Calculate(double operand1, double operand2)
        {
            return operand1 + operand2;
        }
    }

}
