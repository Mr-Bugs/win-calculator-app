﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinCalculatorApp
{
    public class Division : Operator
    {
        public override int Priority => 2;

        public override double Calculate(double operand1, double operand2)
        {
            if (operand2 == 0)
            {
                throw new DivideByZeroException();
            }
            return operand1 / operand2;
        }
    }

}
