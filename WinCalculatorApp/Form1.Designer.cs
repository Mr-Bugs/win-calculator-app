﻿namespace WinCalculatorApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            HisDisplay = new Label();
            Display = new Label();
            Num1 = new Button();
            Num2 = new Button();
            Num3 = new Button();
            Num4 = new Button();
            Num5 = new Button();
            Num0 = new Button();
            Num9 = new Button();
            Num8 = new Button();
            Num7 = new Button();
            Num6 = new Button();
            Calcu = new Button();
            Div = new Button();
            Mult = new Button();
            Sub = new Button();
            Add = new Button();
            Clean = new Button();
            SuspendLayout();
            // 
            // HisDisplay
            // 
            HisDisplay.BorderStyle = BorderStyle.FixedSingle;
            HisDisplay.Location = new Point(39, 24);
            HisDisplay.Name = "HisDisplay";
            HisDisplay.Size = new Size(577, 80);
            HisDisplay.TabIndex = 0;
            // 
            // Display
            // 
            Display.BorderStyle = BorderStyle.FixedSingle;
            Display.Location = new Point(39, 118);
            Display.Name = "Display";
            Display.Size = new Size(577, 82);
            Display.TabIndex = 1;
            // 
            // Num1
            // 
            Num1.Location = new Point(39, 214);
            Num1.Name = "Num1";
            Num1.Size = new Size(139, 49);
            Num1.TabIndex = 2;
            Num1.Text = "1";
            Num1.UseVisualStyleBackColor = true;
            Num1.Click += Num1_Click;
            // 
            // Num2
            // 
            Num2.Location = new Point(184, 214);
            Num2.Name = "Num2";
            Num2.Size = new Size(139, 49);
            Num2.TabIndex = 3;
            Num2.Text = "2";
            Num2.UseVisualStyleBackColor = true;
            Num2.Click += Num2_Click;
            // 
            // Num3
            // 
            Num3.Location = new Point(329, 214);
            Num3.Name = "Num3";
            Num3.Size = new Size(139, 49);
            Num3.TabIndex = 4;
            Num3.Text = "3";
            Num3.UseVisualStyleBackColor = true;
            Num3.Click += Num3_Click;
            // 
            // Num4
            // 
            Num4.Location = new Point(39, 269);
            Num4.Name = "Num4";
            Num4.Size = new Size(139, 49);
            Num4.TabIndex = 5;
            Num4.Text = "4";
            Num4.UseVisualStyleBackColor = true;
            Num4.Click += Num4_Click;
            // 
            // Num5
            // 
            Num5.Location = new Point(184, 269);
            Num5.Name = "Num5";
            Num5.Size = new Size(139, 49);
            Num5.TabIndex = 6;
            Num5.Text = "5";
            Num5.UseVisualStyleBackColor = true;
            Num5.Click += Num5_Click;
            // 
            // Num0
            // 
            Num0.Location = new Point(184, 379);
            Num0.Name = "Num0";
            Num0.Size = new Size(139, 49);
            Num0.TabIndex = 11;
            Num0.Text = "0";
            Num0.UseVisualStyleBackColor = true;
            Num0.Click += Num0_Click;
            // 
            // Num9
            // 
            Num9.Location = new Point(329, 324);
            Num9.Name = "Num9";
            Num9.Size = new Size(139, 49);
            Num9.TabIndex = 10;
            Num9.Text = "9";
            Num9.UseVisualStyleBackColor = true;
            Num9.Click += Num9_Click;
            // 
            // Num8
            // 
            Num8.Location = new Point(184, 324);
            Num8.Name = "Num8";
            Num8.Size = new Size(139, 49);
            Num8.TabIndex = 9;
            Num8.Text = "8";
            Num8.UseVisualStyleBackColor = true;
            Num8.Click += Num8_Click;
            // 
            // Num7
            // 
            Num7.Location = new Point(39, 324);
            Num7.Name = "Num7";
            Num7.Size = new Size(139, 49);
            Num7.TabIndex = 8;
            Num7.Text = "7";
            Num7.UseVisualStyleBackColor = true;
            Num7.Click += Num7_Click;
            // 
            // Num6
            // 
            Num6.Location = new Point(329, 269);
            Num6.Name = "Num6";
            Num6.Size = new Size(139, 49);
            Num6.TabIndex = 7;
            Num6.Text = "6";
            Num6.UseVisualStyleBackColor = true;
            Num6.Click += Num6_Click;
            // 
            // Calcu
            // 
            Calcu.Location = new Point(329, 379);
            Calcu.Name = "Calcu";
            Calcu.Size = new Size(139, 49);
            Calcu.TabIndex = 16;
            Calcu.Text = "=";
            Calcu.UseVisualStyleBackColor = true;
            Calcu.Click += Calcu_Click;
            // 
            // Div
            // 
            Div.Location = new Point(477, 379);
            Div.Name = "Div";
            Div.Size = new Size(139, 49);
            Div.TabIndex = 15;
            Div.Text = "/";
            Div.UseVisualStyleBackColor = true;
            Div.Click += Div_Click;
            // 
            // Mult
            // 
            Mult.Location = new Point(477, 324);
            Mult.Name = "Mult";
            Mult.Size = new Size(139, 49);
            Mult.TabIndex = 14;
            Mult.Text = "x";
            Mult.UseVisualStyleBackColor = true;
            Mult.Click += Mult_Click;
            // 
            // Sub
            // 
            Sub.Location = new Point(477, 269);
            Sub.Name = "Sub";
            Sub.Size = new Size(139, 49);
            Sub.TabIndex = 13;
            Sub.Text = "-";
            Sub.UseVisualStyleBackColor = true;
            Sub.Click += Sub_Click;
            // 
            // Add
            // 
            Add.Location = new Point(477, 214);
            Add.Name = "Add";
            Add.Size = new Size(139, 49);
            Add.TabIndex = 12;
            Add.Text = "+";
            Add.UseVisualStyleBackColor = true;
            Add.Click += Add_Click;
            // 
            // Clean
            // 
            Clean.Location = new Point(39, 379);
            Clean.Name = "Clean";
            Clean.Size = new Size(139, 49);
            Clean.TabIndex = 17;
            Clean.Text = "清除";
            Clean.UseVisualStyleBackColor = true;
            Clean.Click += Clean_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(658, 448);
            Controls.Add(Clean);
            Controls.Add(Calcu);
            Controls.Add(Div);
            Controls.Add(Mult);
            Controls.Add(Sub);
            Controls.Add(Add);
            Controls.Add(Num0);
            Controls.Add(Num9);
            Controls.Add(Num8);
            Controls.Add(Num7);
            Controls.Add(Num6);
            Controls.Add(Num5);
            Controls.Add(Num4);
            Controls.Add(Num3);
            Controls.Add(Num2);
            Controls.Add(Num1);
            Controls.Add(Display);
            Controls.Add(HisDisplay);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
        }

        #endregion

        private Label HisDisplay;
        private Label Display;
        private Button Num1;
        private Button Num2;
        private Button Num3;
        private Button Num4;
        private Button Num5;
        private Button Num0;
        private Button Num9;
        private Button Num8;
        private Button Num7;
        private Button Num6;
        private Button Calcu;
        private Button Div;
        private Button Mult;
        private Button Sub;
        private Button Add;
        private Button Clean;
    }
}