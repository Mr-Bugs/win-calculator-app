﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinCalculatorApp
{
    public abstract class Operator
    {
        public abstract int Priority { get; }

        public abstract double Calculate(double operand1, double operand2);
    }
}
