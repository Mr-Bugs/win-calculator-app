﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinCalculatorApp
{
    using System.Collections.Generic;
    public class Calculator
    {
        private Stack<ICalculable> operandStack;
        private Stack<Operator> operatorStack;

        public Calculator()
        {
            operandStack = new Stack<ICalculable>();
            operatorStack = new Stack<Operator>();
        }

        public double Evaluate(string expression)
        {
            string[] tokens = expression.Split(' ');

            foreach (string token in tokens)
            {
                if (IsNumber(token))
                {
                    operandStack.Push(new Operand(double.Parse(token)));
                    continue;
                }
                else if (IsOperator(token))
                {
                    Operator op = CreateOperator(token);
                    while (operatorStack.Count > 0 && operatorStack.Peek().Priority >= op.Priority)
                    {
                        Operator top = operatorStack.Peek();
                        if (op is Multiplication || op is Division || top is Addition || top is Subtraction)
                        {
                            break;
                        }
                        operatorStack.Pop();
                        ICalculable operand2 = operandStack.Pop();
                        ICalculable operand1 = operandStack.Pop();
                        operandStack.Push(new Operand(top.Calculate(operand1.Calculate(), operand2.Calculate())));
                    }
                    operatorStack.Push(op);
                    continue;
                }
            }

            while (operatorStack.Count > 0)
            {
                Operator op = operatorStack.Pop();
                ICalculable operand2 = operandStack.Pop();
                ICalculable operand1 = operandStack.Pop();
                operandStack.Push(new Operand(op.Calculate(operand1.Calculate(), operand2.Calculate())));
            }

            return operandStack.Pop().Calculate();
        }

        private bool IsNumber(string token)
        {
            double value;
            return double.TryParse(token, out value);
        }

        private bool IsOperator(string token)
        {
            return token == "+" || token == "-" || token == "*" || token == "/";
        }

        private Operator CreateOperator(string token)
        {

            switch (token)
            {
                case "+":
                    return new Addition();
                case "-":
                    return new Subtraction();
                case "*":
                    return new Multiplication();
                case "/":
                    return new Division();
                default:
                    throw new ArgumentException("Invalid operator: " + token);
            }
        }
    }

}
