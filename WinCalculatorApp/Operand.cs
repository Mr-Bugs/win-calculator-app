﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinCalculatorApp
{
    public class Operand : ICalculable
    {
        private double value;

        public Operand(double value)
        {
            this.value = value;
        }
        public double Calculate()
        {
            return value;
        }

    }
}


