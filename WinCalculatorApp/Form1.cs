using System.Linq.Expressions;
using System.Text;

namespace WinCalculatorApp
{
    public partial class Form1 : Form
    {
        public StringBuilder expression = new();

        public Form1()
        {
            InitializeComponent();
        }
        private void PostDisplay()
        {
            this.Display.Text = this.expression.ToString();
        }

        private void Num1_Click(object sender, EventArgs e)
        {
            this.expression.Append("1");
            PostDisplay();
        }

        private void Num2_Click(object sender, EventArgs e)
        {
            this.expression.Append("2");
            PostDisplay();
        }

        private void Num3_Click(object sender, EventArgs e)
        {
            this.expression.Append("3");
            PostDisplay();
        }

        private void Num4_Click(object sender, EventArgs e)
        {
            this.expression.Append("4");
            PostDisplay();
        }

        private void Num5_Click(object sender, EventArgs e)
        {
            this.expression.Append("5");
            PostDisplay();
        }

        private void Num6_Click(object sender, EventArgs e)
        {
            this.expression.Append("6");
            PostDisplay();
        }

        private void Num7_Click(object sender, EventArgs e)
        {
            this.expression.Append("7");
            PostDisplay();
        }

        private void Num8_Click(object sender, EventArgs e)
        {
            this.expression.Append("8");
            PostDisplay();
        }

        private void Num9_Click(object sender, EventArgs e)
        {
            this.expression.Append("9");
            PostDisplay();
        }

        private void Num0_Click(object sender, EventArgs e)
        {
            this.expression.Append("0");
            PostDisplay();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            this.expression.Append(" + ");
            PostDisplay();
        }

        private void Sub_Click(object sender, EventArgs e)
        {
            this.expression.Append(" - ");
            PostDisplay();
        }

        private void Mult_Click(object sender, EventArgs e)
        {
            this.expression.Append(" * ");
            PostDisplay();
        }

        private void Div_Click(object sender, EventArgs e)
        {
            this.expression.Append(" / ");
            PostDisplay();
        }

        private void Calcu_Click(object sender, EventArgs e)
        {
            Calculator calculator = new Calculator();
            double result = calculator.Evaluate(this.expression.ToString());
            expression.Append(" = " + result + "\n");
            PostDisplay();
            this.HisDisplay.Text = expression.ToString();
            expression.Clear();
        }

        private void Clean_Click(object sender, EventArgs e)
        {
            expression.Clear();
            PostDisplay();
//            this.HisDisplay.Text = "";
        }
    }
}