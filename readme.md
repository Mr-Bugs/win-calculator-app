

​		先说一下这个计算器肯定有很多bug，但是其设计的思想是面向对象的，思路是抽象过的，我只是简单写了一下，能够满足一般的加减乘除的。如果完善一下肯定是没问题的

# 抽象

面向对象的思想：将数字和运算符都当作基础的组件（类），运算的时候利用这些组件进行运算；用到了继承封装和多态，组件与组件的耦合度是很低的，如果你想增加新的功能，比如说：增加平方，就可以写一个类实现运算符接口，定义其优先级，然后改一下运算类，运算类可以再优化。

## 接口

首先，我们需要定义一个 `ICalculable` 接口，用于表示可以进行计算的对象，该接口包含一个 `double Calculate()` 方法，用于对对象进行计算。

## 抽象类

接下来，我们定义一个抽象类 `Operator`，用于表示运算符。它包含以下属性和方法：

- `int Priority` 属性：运算符的优先级，类型为 int。值越大表示优先级越高。
- `double Calculate(double leftOperand, double rightOperand)` 方法：对左右操作数进行运算，并返回运算结果。其中 `leftOperand` 和 `rightOperand` 参数是运算符的两个操作数，类型均为 double。

## 加减乘除实现类

接下来，我们定义四个派生类：`Addition`、`Subtraction`、`Multiplication` 和 `Division`，分别表示加法、减法、乘法和除法运算符。这些派生类实现了 `Operator` 类的 `Calculate` 方法，并在构造函数中设置了对应的优先级。

## 数字类

接下来，我们定义一个 `Operand` 类，用于表示数字。它实现了 `ICalculable` 接口，包含以下属性和方法：

- `double Value` 属性：数字的值，类型为 double。
- `double Calculate()` 方法：返回数字本身。

## 运算类

接下来，我们定义一个 `Calculator` 类，用于实现计算器程序。它包含以下方法：

- `double Evaluate(string expression)`：对输入的表达式进行计算，并返回计算结果。接下来，我们在 `Calculator` 类中添加一个 `ParseExpression` 方法，用于将输入的字符串表达式解析成一个 `ICalculable` 对象。这个方法采用递归下降分析的方法，从左到右依次分析表达式中的每个项，并返回一个 `ICalculable` 对象，表示整个表达式的计算结果。